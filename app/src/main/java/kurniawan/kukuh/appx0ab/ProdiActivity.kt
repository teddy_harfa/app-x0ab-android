package kurniawan.kukuh.appx0ab

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_prodi.*
import org.json.JSONArray
import org.json.JSONObject

class ProdiActivity : AppCompatActivity(), View.OnClickListener {

    //inisiasi variabel
    lateinit var prodiAdapter : AdapterDataProdi
    var daftarProdi = mutableListOf<HashMap<String,String>>()
    var url4 = "http://192.168.43.34/kampus/show_data_prodi.php"
    var url5 = "http://192.168.43.34/kampus/query_upd_del_ins_prodi.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prodi)
        prodiAdapter = AdapterDataProdi(daftarProdi,this)
        listProdi.layoutManager = LinearLayoutManager(this)
        listProdi.adapter = prodiAdapter
        btnInsProdi.setOnClickListener(this)
        btnUpProdi.setOnClickListener(this)
        btnDelProdi.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
    }

    fun queryInsertUpdateDeleteProdi(mode : String){
        val request = object : StringRequest(
            Method.POST,url5,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataProdi()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_prodi",edIdProdi.text.toString())
                        hm.put("nama_prodi",edNamaProdi.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_prodi",edIdProdi.text.toString())
                        hm.put("nama_prodi",edNamaProdi.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_prodi",edIdProdi.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataProdi(){
        val request = StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prodi = HashMap<String,String>()
                    prodi.put("id_prodi",jsonObject.getString("id_prodi"))
                    prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    daftarProdi.add(prodi)
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsProdi ->{
                queryInsertUpdateDeleteProdi("insert")
            }
            R.id.btnUpProdi ->{
                queryInsertUpdateDeleteProdi("update")
            }
            R.id.btnDelProdi ->{
                queryInsertUpdateDeleteProdi("delete")
            }
        }
    }

}
package kurniawan.kukuh.appx0ab

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_prodi.*

class AdapterDataProdi(val dataProdi: List<HashMap<String,String>>,
                       val prodiActivity: ProdiActivity) : //new
    RecyclerView.Adapter<AdapterDataProdi.HolderDataProdi>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataProdi.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_prodi,p0,false)
        return HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataProdi.size
    }

    override fun onBindViewHolder(p0: AdapterDataProdi.HolderDataProdi, p1: Int) {
        val data = dataProdi.get(p1)
        p0.txIdProdi.setText(data.get("id_prodi"))
        p0.txNamaProdi.setText(data.get("nama_prodi"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout2.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout2.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout2.setOnClickListener(View.OnClickListener {
            prodiActivity.edIdProdi.setText(data.get("id_prodi"))
            prodiActivity.edNamaProdi.setText(data.get("nama_prodi"))
        })
        //endNew
    }

    class HolderDataProdi(v: View) : RecyclerView.ViewHolder(v){
        val txIdProdi = v.findViewById<TextView>(R.id.txIdProdi)
        val txNamaProdi = v.findViewById<TextView>(R.id.txNamaProdi)
        val cLayout2 = v.findViewById<ConstraintLayout>(R.id.cLayout2) //new
    }
}